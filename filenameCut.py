import shutil
import os

class FilenameCut:

    def __init__(self, folders):
        self.folders = folders
        self.TEXTFILE_PATH = 'C:/Programming/python/FilenameCut/txt/list.txt'

    def filename_long_out(self):
        self.file_create()
        for folder in self.folders:
            folder_path = folder
            _files = self.get_files(folder)
            for file in _files:
                _name, _ext = os.path.splitext(file)
                if self.is_video(_ext):
                    if self.is_filename_long(_name, 227):
                        self.file_out(_name,folder_path)

    def file_out(self,str,folder_path):
        path_w = self.TEXTFILE_PATH
        str = '\r' + folder_path + ',' + str
        with open(path_w, mode='a') as f:
            f.write(str)

    def file_create(self):
        path_w = self.TEXTFILE_PATH
        with open(path_w, mode='w') as f:
            pass

    def is_video(self,ext):
        if ext == '.mp4' or ext == '.MP4' or ext == '.wmv' or ext == '.WMV' or ext == '.mkv' or ext == '.MKV':
            return True
        else:
            return False
    def get_files(self, folder_path):
        _items = os.listdir(folder_path)
        return list(filter(lambda file: os.path.isfile(folder_path + file) , _items))
    def get_folders(self, folder_path):
        _items = os.listdir(folder_path)
        return list(filter(lambda file: os.path.isdir(folder_path + file), _items))
    def truncate(self,str, num_bytes, encoding='cp932'):
        while len(str.encode(encoding)) > num_bytes:
            str = str[:-1]
        return str
    def is_filename_long(self,str, num_bytes, encoding='cp932'):
        if len(str.encode(encoding)) > num_bytes:
            return True
        return False

